const Leite = require('leite');
const leite = new Leite

// var webdriver = require('selenium-webdriver');

// var userName = "fernandatavares1";
// var accessKey = "jbQgjxFE4A95CrqExU1x"
// var browserstackURL = 'https://' + userName + ':' + accessKey + '@hub-cloud.browserstack.com/wd/hub';

// var capabilities = {
//   'os_version' : '11',
//   'device' : 'iPhone 8 Plus',
//   'real_mobile' : 'true',
//   'browserstack.local' : 'false',
//   'browserName' : 'iPhone',
//   'name': 'My First Test',
//   'build': 'Build #1',
//   'project': 'Sample sandbox project'
// }

// var driver = new webdriver.Builder().
// usingServer(browserstackURL).
// withCapabilities(capabilities).
// build();

// driver.get('https://ead.pucpr.br/inscricao?utm_source=teste&utm_medium=campus');
// driver.quit();

describe('Acessar o site da IES PUCPR.', () => {

  it('Visitar IES', () => {
    browser.url("https://ead.pucpr.br/inscricao?utm_source=teste&utm_medium=campus")
  })
  describe('Popular os campos da etapa', () => {
    it('Inserir primeiro nome', () => {
      firstName = $('input[name="firstname"]')
      firstName.setValue(leite.pessoa.primeiroNome())
    })
    it('Inserir sobrenome', () => {
      lastName = $('input[name="lastname"]')
      lastName.setValue(leite.pessoa.sobrenome() + " TESTE")
    })
    it('Inserir email', () => {
      // cy
      //   .server()
      //   .route('POST', '/emailcheck/v1/*').as('emailCheck')
      email = $('input[name="email"]')
      email.setValue('teste' + leite.pessoa.email())
      // .wait('@emailCheck').its('status').should('eq', 200)
    })
    it('Inserir telefone', () => {
      // cy
      //   .server()
      //   .route('POST', '/emailcheck/v1/*').as('emailCheck')
      phone = $('input[name="phone"]')
      phone.setValue(`11 988392283`)
      // .wait('@emailCheck').its('status').should('eq', 200)
    })
    it('Autorizar uso do Whatsapp', () => {
      whatsapp = $('input[name="podemos_usar_o_whatsapp_para_falar_sobre_sua_inscricao_e_matricula_"]')
      whatsapp.click()
    })
    it('Enviar dados', () => {
      submit = $('input[type="submit"]')
      submit.click()
    })
  })
});

describe('Inscrição no curso', () => {
  describe('Escolha do curso', () => {
    it('Forma de ingresso', () => {
      // cy
      //   .server()
      //   .route('GET', '/v1/subscription/courses*').as('getCourses')
      courseEntrance = $('.v-slide-group__content > span').click()
        // .wait('@getCourses').its('status').should('eq', 200)
    });
    it('Selecionar o curso Administração', () => {
      // cy
      //   .server()
      //   .route('GET', '/v1/subscription/campuses*').as('getCampuses')
      course = $('.v-select__slot > input[id="selCoursePickerCourse"]').click()
      selectCourse = $('.v-select-list > .v-list-item').click()
        // .wait('@getCampuses').its('status').should('eq', 200)
    });
    it('Selecionar o local/campus', () => {
      campus = $('#selCoursePickerCampus').click()
      select = $('div[role="listbox"] > div[role="option"]')
    })
    // it('Continuar inscrição', () => {
    //   cy
    //     .server()
    //     .route('POST', '/v1/subscription/institutions/7/deals').as('postInscrition')
    //   cy
    //     .get('button[id="btnContinueCRS"]')
    //     .click()
    //     .wait('@postInscrition').its('status').should('eq', 200)
    // });
  })
});